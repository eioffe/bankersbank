﻿using System;

namespace BankersBankDataGrid.DataModels
{
    public class Account
    {
        public int AccountNumber { get; set; }
        public string LastNameOrCompanyName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumberPrimary { get; set; }
        public string PhoneNumberSecondary { get; set; }
        public DateTime DateAccountOpened { get; set; }
        public decimal CurrentBalance { get; set; }
        public string AccountType { get; set; }
    }
}