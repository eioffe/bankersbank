﻿using System;
using BankersBankDataGrid.DataModels;

namespace BankersBankDataGrid.BusinessLogic
{
    public class AccountMapper : IMapper<Account>
    {
        public Account Map(string[] StringArray)
        {
            var account = new Account
            {
                AccountNumber = int.Parse(StringArray[0]),
                LastNameOrCompanyName = StringArray[1],
                FirstName = StringArray[2],
                MiddleName = StringArray[3],
                Address1 = StringArray[4],
                Address2 = StringArray[5],
                City = StringArray[6],
                State = StringArray[7],
                ZipCode = StringArray[8],
                PhoneNumberPrimary = StringArray[9],
                PhoneNumberSecondary = StringArray[10],
                DateAccountOpened = DateTime.Parse(StringArray[11]),
                CurrentBalance = decimal.Parse(StringArray[12]),
                AccountType = StringArray[13]
            };

            return account;
        }
    }
}