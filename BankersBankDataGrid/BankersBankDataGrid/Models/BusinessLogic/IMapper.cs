﻿namespace BankersBankDataGrid.BusinessLogic
{
    public interface IMapper<T>
    {
        T Map(string[] StringArray);
    }
}
