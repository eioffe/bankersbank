﻿using BankersBankDataGrid.BusinessLogic;
using BankersBankDataGrid.DataModels;
using BankersBankDataGrid.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;

namespace BBankersBankDataGrid.BusinessLogic
{
    public class AccountImporter
    {
        private string filePath;
        private char delimiter;
        private ILogger logger;
        private IList<Account> accountList = new List<Account>();
        private IMapper<Account> accountMapper = new AccountMapper();

        public AccountImporter(string FilePath, Char Delimiter, ILogger Logger)
        {
            filePath = FilePath;
            delimiter = Delimiter;
            logger = Logger;
        }

        public IEnumerable<Account> GetData()
        {
            var successCount = 0;
            var errorCount = 0;

            using (var streamReader = new StreamReader(filePath))
            {
                //TODO: For peak performance, we can try getting all the data in one shot and then parse it
                while (!streamReader.EndOfStream)
                {
                    var stringArray = streamReader.ReadLine().Split(delimiter);

                    try
                    {
                        accountList.Add(accountMapper.Map(stringArray));
                        successCount++;
                    }
                    catch (Exception)
                    {
                        //Do we wish to log the specific exception?
                        errorCount++;
                    }
                };
            };

            //Log the date of import, number of successful records, and number of errors
            logger.WriteToLog(string.Format("Import completed on {0}: {1} successful record(s), {2} error(s).",
                    DateTime.Now.ToString(), successCount, errorCount));

            return accountList;
        }
    }
}