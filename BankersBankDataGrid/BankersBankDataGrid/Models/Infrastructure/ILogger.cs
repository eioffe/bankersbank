﻿namespace BankersBankDataGrid.Infrastructure
{
    public interface ILogger
    {
        void WriteToLog(string Message);
    }
}