﻿using System.IO;

namespace BankersBankDataGrid.Infrastructure
{
    public class FileLogger : ILogger
    {
        private string filePath;

        public FileLogger(string FilePath)
        {
            filePath = FilePath;
        }

        public void WriteToLog(string Message)
        {
            using (StreamWriter streamWriter = new StreamWriter(filePath, true))
            {
                streamWriter.WriteLine(Message);
            };
        }
    }
}