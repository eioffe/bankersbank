﻿namespace BankersBankDataGrid.Infrastructure
{
    public static class Delimiter
    {
        public static char Comma
        {
            get { return ','; }
        }
    }
}