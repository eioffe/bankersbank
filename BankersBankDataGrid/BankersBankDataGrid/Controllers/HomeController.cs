﻿using BankersBankDataGrid.Infrastructure;
using BBankersBankDataGrid.BusinessLogic;
using System.Web.Mvc;

namespace BankersBankDataGrid.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var importPath = Server.MapPath("~/App_Data/Import File 1 FOR TEST.csv");
            var logPath = Server.MapPath("~/App_Data/ImportLog.txt");

            //inject logger service
            //import data from file
            //get back an Account enumerable

            var logger = new FileLogger(logPath);
            var importer = new AccountImporter(importPath, Delimiter.Comma, logger);
            var data = importer.GetData();

            return View(data);
        }
    }
}